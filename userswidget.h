#ifndef USERSWIDGET_H
#define USERSWIDGET_H

#include <QWidget>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDesktopWidget>

namespace Ui {
class UsersWidget;
}

class UsersWidget : public QWidget
{
    Q_OBJECT

public:
    explicit UsersWidget(QWidget *parent = 0);
    ~UsersWidget();
    void populate(QJsonObject jsonObj);
    void updateSize(QSize desktopSize);
    void updateUsers(QJsonObject jsonObj);

signals:
    void sendLogin(QString userId,QString roleId,QString userName);

private slots:
    void on_pushButton_clicked();
    void on_comboBox_currentIndexChanged(const QString &arg1);

private:
    Ui::UsersWidget *ui;
    QJsonArray dataArray;
};

#endif // USERSWIDGET_H
