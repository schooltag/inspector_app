#include "infowidget.h"
#include "ui_infowidget.h"

InfoWidget::InfoWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InfoWidget)
{
    ui->setupUi(this);

}

InfoWidget::~InfoWidget()
{
    delete ui;
}

void InfoWidget::populate(QJsonObject jsonObjData){
    QString text = jsonObjData["text"].toString();
    QString title = jsonObjData["title"].toString();

    ui->label_title->setText(title);
    ui->label_text->setText(text);

}


void InfoWidget::on_overlay_button_clicked()
{
    emit sendClose();
}
