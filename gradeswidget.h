#ifndef GRADESWIDGET_H
#define GRADESWIDGET_H

#include <QWidget>
#include <QList>
#include <QPushButton>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QDesktopWidget>

namespace Ui {
class GradesWidget;
}

class GradesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit GradesWidget(QWidget *parent = 0);
    ~GradesWidget();
    void populate(QJsonObject jsonObj);
    void unselect();
    void updateSize(QSize desktopSize);
    void setUserName(QString userName);


signals:
    void sendGrade(QString gradeId,QString gradeName, QString isEntry);
    void sendClose();
    void sendDrop();

private slots:
    void on_curso_pushButton_clicked();
    void on_par_pushButton_clicked();
    void on_consultar_pushButton_clicked();
    void on_back_clicked();
    void on_consultar_pushButton_2_clicked();

    void on_emergency_clicked();

private:
    Ui::GradesWidget *ui;
    QJsonObject jsonObjData;
    QJsonObject jsonObjGradeSeleted;
    QString selectedGradeId;
    QString selectedGradeName;
    QList <QPushButton*> cursos_pushButton_list;
    QList <QPushButton*> par_pushButton_list_4;
    QList <QPushButton*> par_pushButton_list_5;
    QList <QPushButton*> par_pushButton_list_6;
    QList <QPushButton*> par_pushButton_list_7;
    QList <QPushButton*> par_pushButton_list_8;
};

#endif // GRADESWIDGET_H
