#include "gradeswidget.h"
#include "ui_gradeswidget.h"

GradesWidget::GradesWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GradesWidget)
{
    ui->setupUi(this);

    int width = QDesktopWidget().width();
    int height = QDesktopWidget().height();

    resize(width,height);
    ui->frame->resize(width,height);

    connect(ui->curso_pushButton_1,SIGNAL(pressed()),this,SLOT(on_curso_pushButton_clicked()));
    connect(ui->curso_pushButton_2,SIGNAL(pressed()),this,SLOT(on_curso_pushButton_clicked()));
    connect(ui->curso_pushButton_3,SIGNAL(pressed()),this,SLOT(on_curso_pushButton_clicked()));
    connect(ui->curso_pushButton_4,SIGNAL(pressed()),this,SLOT(on_curso_pushButton_clicked()));
    connect(ui->curso_pushButton_5,SIGNAL(pressed()),this,SLOT(on_curso_pushButton_clicked()));
    connect(ui->curso_pushButton_6,SIGNAL(pressed()),this,SLOT(on_curso_pushButton_clicked()));
    connect(ui->curso_pushButton_7,SIGNAL(pressed()),this,SLOT(on_curso_pushButton_clicked()));
    connect(ui->curso_pushButton_8,SIGNAL(pressed()),this,SLOT(on_curso_pushButton_clicked()));
    connect(ui->curso_pushButton_9,SIGNAL(pressed()),this,SLOT(on_curso_pushButton_clicked()));
    connect(ui->curso_pushButton_10,SIGNAL(pressed()),this,SLOT(on_curso_pushButton_clicked()));
    connect(ui->curso_pushButton_11,SIGNAL(pressed()),this,SLOT(on_curso_pushButton_clicked()));
    connect(ui->curso_pushButton_12,SIGNAL(pressed()),this,SLOT(on_curso_pushButton_clicked()));


    connect(ui->par_pushButton_1,SIGNAL(pressed()),this,SLOT(on_par_pushButton_clicked()));
    connect(ui->par_pushButton_2,SIGNAL(pressed()),this,SLOT(on_par_pushButton_clicked()));
    connect(ui->par_pushButton_3,SIGNAL(pressed()),this,SLOT(on_par_pushButton_clicked()));
    connect(ui->par_pushButton_4,SIGNAL(pressed()),this,SLOT(on_par_pushButton_clicked()));
    connect(ui->par_pushButton_5,SIGNAL(pressed()),this,SLOT(on_par_pushButton_clicked()));
    connect(ui->par_pushButton_6,SIGNAL(pressed()),this,SLOT(on_par_pushButton_clicked()));
    connect(ui->par_pushButton_7,SIGNAL(pressed()),this,SLOT(on_par_pushButton_clicked()));
    connect(ui->par_pushButton_8,SIGNAL(pressed()),this,SLOT(on_par_pushButton_clicked()));

    cursos_pushButton_list.append(ui->curso_pushButton_1);
    cursos_pushButton_list.append(ui->curso_pushButton_2);
    cursos_pushButton_list.append(ui->curso_pushButton_3);
    cursos_pushButton_list.append(ui->curso_pushButton_4);
    cursos_pushButton_list.append(ui->curso_pushButton_5);
    cursos_pushButton_list.append(ui->curso_pushButton_6);
    cursos_pushButton_list.append(ui->curso_pushButton_7);
    cursos_pushButton_list.append(ui->curso_pushButton_8);
    cursos_pushButton_list.append(ui->curso_pushButton_9);
    cursos_pushButton_list.append(ui->curso_pushButton_10);
    cursos_pushButton_list.append(ui->curso_pushButton_11);
    cursos_pushButton_list.append(ui->curso_pushButton_12);

    par_pushButton_list_4.append(ui->par_pushButton_1);
    par_pushButton_list_4.append(ui->par_pushButton_2);
    par_pushButton_list_4.append(ui->par_pushButton_3);
    par_pushButton_list_4.append(ui->par_pushButton_4);

    par_pushButton_list_5.append(ui->par_pushButton_1);
    par_pushButton_list_5.append(ui->par_pushButton_2);
    par_pushButton_list_5.append(ui->par_pushButton_3);
    par_pushButton_list_5.append(ui->par_pushButton_5);
    par_pushButton_list_5.append(ui->par_pushButton_6);

    par_pushButton_list_6.append(ui->par_pushButton_1);
    par_pushButton_list_6.append(ui->par_pushButton_2);
    par_pushButton_list_6.append(ui->par_pushButton_3);
    par_pushButton_list_6.append(ui->par_pushButton_5);
    par_pushButton_list_6.append(ui->par_pushButton_6);
    par_pushButton_list_6.append(ui->par_pushButton_7);

    par_pushButton_list_7.append(ui->par_pushButton_1);
    par_pushButton_list_7.append(ui->par_pushButton_2);
    par_pushButton_list_7.append(ui->par_pushButton_3);
    par_pushButton_list_7.append(ui->par_pushButton_4);
    par_pushButton_list_7.append(ui->par_pushButton_5);
    par_pushButton_list_7.append(ui->par_pushButton_6);
    par_pushButton_list_7.append(ui->par_pushButton_7);

    par_pushButton_list_8.append(ui->par_pushButton_1);
    par_pushButton_list_8.append(ui->par_pushButton_2);
    par_pushButton_list_8.append(ui->par_pushButton_3);
    par_pushButton_list_8.append(ui->par_pushButton_4);
    par_pushButton_list_8.append(ui->par_pushButton_5);
    par_pushButton_list_8.append(ui->par_pushButton_6);
    par_pushButton_list_8.append(ui->par_pushButton_7);
    par_pushButton_list_8.append(ui->par_pushButton_8);

    QList <QWidget*> listaWidgets = this->findChildren<QWidget*>();
    for(int i = 0; i < listaWidgets.size(); i++){
        listaWidgets.at(i)->clearFocus();
        listaWidgets.at(i)->setFocusPolicy(Qt::NoFocus);
    }

}

GradesWidget::~GradesWidget()
{
    delete ui;
}

void GradesWidget::populate(QJsonObject jsonObj){
    jsonObjData = jsonObj;
    QJsonArray jsonArray = jsonObj["gradesList"].toArray();
    QList<QString> gradeNames;
    int listSize = jsonArray.size();
    for(int i = 0; i < 12; i++){
        cursos_pushButton_list[i]->hide();
        cursos_pushButton_list[i]->setChecked(false);
        cursos_pushButton_list[i]->setText("nada");
    }
    for(int i = 0; i < 8; i++){
        par_pushButton_list_8[i]->hide();
        par_pushButton_list_8[i]->setChecked(false);
    }
    for(int i = 0; i < listSize; i++){
        QString gradeName = jsonArray[i].toObject()["name"].toString();
        if(!gradeNames.contains(gradeName)) gradeNames.append(gradeName);
    }

    for(int i = 0; i < gradeNames.size(); i++){
        cursos_pushButton_list[i]->show();
        cursos_pushButton_list[i]->setText(gradeNames[i]);
    }


    ui->consultar_pushButton->setEnabled(false);
    ui->consultar_pushButton_2->setEnabled(false);
}

void GradesWidget::setUserName(QString userName){
    ui->userLabel->setText(userName);
}

void GradesWidget::unselect(){
    for(int i = 0; i < 12; i++){
        cursos_pushButton_list[i]->setChecked(false);
    }
    for(int i = 0; i < 8; i++){
        par_pushButton_list_8[i]->hide();
        par_pushButton_list_8[i]->setChecked(false);
    }
    ui->consultar_pushButton->setEnabled(false);
    ui->consultar_pushButton_2->setEnabled(false);
}

void GradesWidget::on_curso_pushButton_clicked(){
    QJsonArray jsonGradeArray = jsonObjData["gradesList"].toArray(), jsonParArray;
    QString gradeName = qobject_cast<QPushButton*>(QObject::sender())->text();
    selectedGradeName = gradeName;
    int parsSize;
    ui->consultar_pushButton->setEnabled(false);
    ui->consultar_pushButton_2->setEnabled(false);
    for(int i = 0; i < 12; i++){
        cursos_pushButton_list[i]->setChecked(false);
    }
    for(int i = 0; i < 8; i++){
        par_pushButton_list_8[i]->hide();
        par_pushButton_list_8[i]->setChecked(false);
    }

    for(int i = 0; i < jsonGradeArray.size(); i++){
        if(jsonGradeArray[i].toObject()["name"] == gradeName) jsonParArray.append(jsonGradeArray[i].toObject());
    }

    parsSize = jsonParArray.size();

    for(int i = 0; i < parsSize; i++){
        if(parsSize < 5){
            QString tmp = jsonParArray[i].toObject()["id"].toString();
            par_pushButton_list_4[i]->show();
            par_pushButton_list_4[i]->setText(jsonParArray[i].toObject()["par"].toString());
            par_pushButton_list_4[i]->setProperty("id",tmp);
        }

        if(parsSize == 5){
            par_pushButton_list_5[i]->show();
            par_pushButton_list_5[i]->setText(jsonParArray[i].toObject()["par"].toString());
            par_pushButton_list_5[i]->setProperty("id",jsonParArray[i].toObject()["id"].toString());
        }

        if(parsSize == 6){
            par_pushButton_list_6[i]->show();
            par_pushButton_list_6[i]->setText(jsonParArray[i].toObject()["par"].toString());
            par_pushButton_list_6[i]->setProperty("id",jsonParArray[i].toObject()["id"].toString());
        }

        if(parsSize == 7){
            par_pushButton_list_7[i]->show();
            par_pushButton_list_7[i]->setText(jsonParArray[i].toObject()["par"].toString());
            par_pushButton_list_7[i]->setProperty("id",jsonParArray[i].toObject()["id"].toString());
        }

        if(parsSize > 7){
            par_pushButton_list_8[i]->show();
            par_pushButton_list_8[i]->setText(jsonParArray[i].toObject()["par"].toString());
            par_pushButton_list_8[i]->setProperty("id",jsonParArray[i].toObject()["id"].toString());
        }
    }




}

void GradesWidget::on_par_pushButton_clicked(){
    QJsonArray jsonArrayPars = jsonObjGradeSeleted["parsList"].toArray();
    QPushButton* pushButton_pressed = qobject_cast<QPushButton*>(QObject::sender());

    for(int i = 0; i < 8; i++){
        par_pushButton_list_8[i]->setChecked(false);
    }

    ui->consultar_pushButton->setEnabled(true);
    ui->consultar_pushButton_2->setEnabled(true);

    selectedGradeId = pushButton_pressed->property("id").toString();
    selectedGradeName = selectedGradeName + " " + pushButton_pressed->text();
}


void GradesWidget::on_consultar_pushButton_clicked()
{
    emit sendGrade(selectedGradeId,selectedGradeName,"1");
}


void GradesWidget::on_back_clicked()
{
   emit sendClose();
}

void GradesWidget::updateSize(QSize desktopSize){
    resize(desktopSize);
    ui->frame->resize(desktopSize);
}

void GradesWidget::on_consultar_pushButton_2_clicked()
{
    emit sendGrade(selectedGradeId,selectedGradeName,"0");
}

void GradesWidget::on_emergency_clicked()
{
    emit sendDrop();
}
