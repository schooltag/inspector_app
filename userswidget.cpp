#include "userswidget.h"
#include "ui_userswidget.h"

UsersWidget::UsersWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UsersWidget)
{
    ui->setupUi(this);

    int width = QDesktopWidget().width();
    int height = QDesktopWidget().height();

    resize(width,height);
    ui->frame->resize(width,height);

    QList <QWidget*> listaWidgets = this->findChildren<QWidget*>();
    for(int i = 0; i < listaWidgets.size(); i++){
        listaWidgets.at(i)->clearFocus();
        listaWidgets.at(i)->setFocusPolicy(Qt::NoFocus);
    }
}

UsersWidget::~UsersWidget()
{
    delete ui;
}

void UsersWidget::populate(QJsonObject jsonObj){
    QString fullname;
    QStringList roleList,roleListId;
    dataArray = jsonObj["userList"].toArray();
    ui->comboBox->blockSignals(true);
    ui->comboBox->clear();
    ui->comboBox_2->clear();



    for(int i = 0; i < dataArray.size(); i++){
        QJsonObject obj = dataArray.at(i).toObject();
        QString roleString = obj["role_name"].toString();
        if(!roleList.contains(roleString)){
            roleList.append(roleString);
            ui->comboBox->addItem(roleString,obj["role_id"].toString());
        }
    }

    if(ui->comboBox->count() == 0){
        return;
    }

    ui->comboBox->setCurrentIndex(0);
    on_comboBox_currentIndexChanged(ui->comboBox->currentText());

    ui->comboBox->blockSignals(false);
}



void UsersWidget::on_pushButton_clicked()
{
    emit sendLogin(ui->comboBox_2->currentData().toString(),ui->comboBox->currentData().toString(),ui->comboBox_2->currentText());
}

void UsersWidget::updateSize(QSize desktopSize){
    resize(desktopSize);
    ui->frame->resize(desktopSize);
}


void UsersWidget::on_comboBox_currentIndexChanged(const QString &roleText)
{
    ui->comboBox_2->clear();
    for(int i = 0; i < dataArray.size(); i++){
        QJsonObject obj = dataArray.at(i).toObject();
        QString roleString = obj["role_name"].toString();
        if(roleString == roleText){
            QString fullname = obj["user_name"].toString() + ' ' + obj["user_last_name"].toString();
            ui->comboBox_2->addItem(fullname,obj["user_id"].toString());
        }
    }

    ui->comboBox_2->setCurrentIndex(0);
}
