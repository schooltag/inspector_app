#include "confirmationwidget.h"
#include "ui_confirmationwidget.h"

ConfirmationWidget::ConfirmationWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConfirmationWidget)
{
    ui->setupUi(this);
    timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(timeout()));
}

ConfirmationWidget::~ConfirmationWidget()
{
    delete ui;
}

void ConfirmationWidget::populate(QString studentName, QString className, QString reasonText,QJsonObject jsonObj){

    if(jsonObj["allowed"].toString() == "1"){
        ui->symbol->setStyleSheet("background-image: url(:/res/ok.png);");
        if(jsonObj["isEntry"].toString() == "1"){
            ui->label_1->setText("Se autoriza la entrada a");
        }else{
            ui->label_1->setText("Se autoriza la salida a");
        }
        ui->label_2->setText(studentName);
        ui->label_3->setText(className);
        ui->label_4->setText("por " + reasonText);
    }else{
        ui->symbol->setStyleSheet("background-image: url(:/res/fail.png);");
        ui->label_1->setText("No se encuentra autorizado");
        ui->label_2->setText("para realizar dicha operación");
        ui->label_3->clear();
        ui->label_4->clear();
    }

    timer->start(6000);
    ui->overlay->clearFocus();
}

void ConfirmationWidget::timeout(){
    timer->stop();
    emit sendClose();
}

void ConfirmationWidget::on_overlay_clicked(){
    timer->stop();
    emit sendClose();
}

