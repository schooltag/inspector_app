#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QHostAddress>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QTimer>
#include <QMovie>
#include <QDesktopWidget>
#include <QNetworkInterface>
#include "gradeswidget.h"
#include "studentswidget.h"
#include "userswidget.h"
#include "accesswidget.h"
#include "confirmationwidget.h"
#include "dropwidget.h"
#include "infowidget.h"
#include "reasonswidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void readyRead();
    void connected();
    void login(QString userId, QString roleId, QString userName);
    void grade(QString gradeId, QString gradeName, QString isEntry);
    void student(QString studentId,QString name);
    void access(QString accessId,QString text,QString id);
    void closeConfirmation();
    void closeGrades();
    void closeStudents();
    void closeAccesses();
    void disconnected();
    void timerInitTimeout();
    void receptionTimerTimeout();

private slots:
    void on_refreshButton_clicked();
    void showOverlay();
    void hideOverlay();
    void showDrop();
    void hideDrop();
    void showInfo();
    void confirmInfo();
    void hideInfo();
    void showConfirmation();
    void hideConfirmation();
    void back2Grade();
    void showReason();
    void hideReason();
    void sendReason(QString reasonId,QString reasonText);
    void sendDrop();
    void selectAccess(QString accessId);

private:
    void resizeEvent(QResizeEvent *event);
    Ui::MainWindow *ui;
    GradesWidget* gradesWigdet;
    StudentsWidget* studentsWidget;
    UsersWidget* usersWidget;
    AccessWidget* accessWidget;
    QTcpSocket* socket;
    DropWidget* dropWidget;
    InfoWidget* infoWidget;
    ConfirmationWidget* confirmationWidget;
    ReasonsWidget* reasonWidget;
    QString deviceId;
    QString userId;
    QString gradeId;
    QString studentId;
    QString roleId;
    QString messageId;
    QString isEntry;
    QString accessId;
    QString gradeName, studentName, reasonText,userName;
    QString myip;
    QTimer* timerInit,* receptionTimer;
    void tryToConnect();
    QMovie* loadingGif;
    QByteArray receptionBuffer;
    bool isOverlayShown,isDropShown,isInfoShown,isConfirmationShown,isReasonShown;
};

#endif // MAINWINDOW_H
