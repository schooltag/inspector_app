#include "accesswidget.h"
#include "ui_accesswidget.h"
#include <QScroller>

AccessWidget::AccessWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AccessWidget)
{
    ui->setupUi(this);


    QScroller::grabGesture(ui->scrollArea, QScroller::LeftMouseButtonGesture);

    QList <QWidget*> listaWidgets = this->findChildren<QWidget*>();
    for(int i = 0; i < listaWidgets.size(); i++){
        listaWidgets.at(i)->clearFocus();
        listaWidgets.at(i)->setFocusPolicy(Qt::NoFocus);
    }

}

AccessWidget::~AccessWidget()
{
    delete ui;
}

void AccessWidget::clearButtons(){
    QLayoutItem *child;
    while ((child = ui->scrollAreaWidgetContents->layout()->takeAt(0)) != 0) {
       delete child->widget();
    }
}

void AccessWidget::addButton(QString labelText,QString id){
    QLabel* label = new QLabel();
    //label->setText(jsonArray[i].toObject()["access_area_name"].toString() + " - " +  jsonArray[i].toObject()["device_name"].toString());
    label->setText(labelText);

    QPushButton* button = new QPushButton();
    connect(button,SIGNAL(clicked(bool)),this,SLOT(accessClicked()));

    if(isEntry == "1")button->setText("Entrada");
    else button->setText("Salida");

    button->setProperty("id",id);

    button->setMinimumHeight(100);
    button->setMaximumWidth(300);
    button->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Maximum);
    button->setFocusPolicy(Qt::NoFocus);

    QHBoxLayout* hbox = new QHBoxLayout();
    hbox->addWidget(label);
    hbox->addWidget(button);
    QFrame* fram = new QFrame();
    fram->setLayout(hbox);
    ui->scrollAreaWidgetContents->layout()->addWidget(fram);
}

void AccessWidget::setEntry(QString isEntry){
    this->isEntry = isEntry;
    clearButtons();

    if(isEntry == "1") ui->studentLabel->setText("Seleccione entrda");
    else ui->studentLabel->setText("Seleccione salida");

    QStringList areaNameList;
    for(int i = 0 ; i < dataArray.size(); i++){
        QJsonObject obj = dataArray.at(i).toObject();
        QString areaString = obj["access_area_name"].toString();
        if(!areaNameList.contains(areaString)){
            areaNameList.append(areaString);
        }
    }

    for(int i = 0; i < areaNameList.size(); i++){
        QString areaName = areaNameList.at(i);
        for(int j = 0; j < dataArray.size(); j++){
            QJsonObject obj = dataArray.at(j).toObject();
            if(obj["access_area_name"].toString() == areaName){
                QString labelText = obj["access_area_name"].toString() + " - " + obj["device_name"].toString();
                QString id = obj["id"].toString();
                addButton(labelText,id);
            }
        }
    }

}

void AccessWidget::populate(QJsonObject jsonObj){

    dataArray = jsonObj["accessesList"].toArray();


}

void AccessWidget::accessClicked(){
    selectedAccess = QObject::sender()->property("id").toString();
    emit sendAccess(selectedAccess);
}

void AccessWidget::on_back_clicked()
{
    emit sendClose();
}


void AccessWidget::updateSize(QSize desktopSize){
    resize(desktopSize);
    ui->frame->resize(desktopSize);

}

void AccessWidget::on_emergency_clicked()
{
    emit sendDrop();
}
