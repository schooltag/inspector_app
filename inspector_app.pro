#-------------------------------------------------
#
# Project created by QtCreator 2017-04-25T19:46:35
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = inspector_app
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    userswidget.cpp \
    gradeswidget.cpp \
    studentswidget.cpp \
    accesswidget.cpp \
    confirmationwidget.cpp \
    dropwidget.cpp \
    infowidget.cpp \
    reasonswidget.cpp

HEADERS  += mainwindow.h \
    userswidget.h \
    gradeswidget.h \
    studentswidget.h \
    accesswidget.h \
    confirmationwidget.h \
    dropwidget.h \
    infowidget.h \
    reasonswidget.h

FORMS    += mainwindow.ui \
    userswidget.ui \
    gradeswidget.ui \
    studentswidget.ui \
    accesswidget.ui \
    confirmationwidget.ui \
    dropwidget.ui \
    infowidget.ui \
    reasonswidget.ui

CONFIG += mobility
MOBILITY = 

RESOURCES += \
    resources.qrc

