#ifndef CONFIRMATIONWIDGET_H
#define CONFIRMATIONWIDGET_H

#include <QWidget>
#include <QJsonObject>
#include <QTimer>
#include <QDesktopWidget>

namespace Ui {
class ConfirmationWidget;
}

class ConfirmationWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ConfirmationWidget(QWidget *parent = 0);
    ~ConfirmationWidget();
    void populate(QString studentName, QString className, QString reasonText, QJsonObject jsonObj);

signals:
    void sendClose();

private slots:
    void timeout();
    void on_overlay_clicked();

private:
    Ui::ConfirmationWidget *ui;
    QTimer* timer;
};

#endif // CONFIRMATIONWIDGET_H
