#include "studentswidget.h"
#include "ui_studentswidget.h"
#include <QScroller>

StudentsWidget::StudentsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StudentsWidget)
{
    ui->setupUi(this);

    int width = QDesktopWidget().width();
    int height = QDesktopWidget().height();

    resize(width,height);
    ui->frame->resize(width,height);

    QScroller::grabGesture(ui->scrollArea, QScroller::LeftMouseButtonGesture);

    QList <QWidget*> listaWidgets = this->findChildren<QWidget*>();
    for(int i = 0; i < listaWidgets.size(); i++){
        listaWidgets.at(i)->clearFocus();
        listaWidgets.at(i)->setFocusPolicy(Qt::NoFocus);
    }
}

StudentsWidget::~StudentsWidget()
{
    delete ui;
}

void StudentsWidget::populate(QJsonObject jsonObj){


    QLayoutItem *child;
    while ((child = ui->scrollAreaWidgetContents->layout()->takeAt(0)) != 0) {
       delete child->widget();
    }

    QJsonArray jsonArray = jsonObj["studentsList"].toArray();
    unsigned int listSize = jsonArray.size();


    for(unsigned int i = 0; i < listSize; i++){
        QPushButton* button = new QPushButton();
        QString fullname = jsonArray[i].toObject()["name"].toString() + " " + jsonArray[i].toObject()["last_name"].toString();
        connect(button,SIGNAL(clicked(bool)),this,SLOT(studentClicked()));
        button->setText(fullname);

        button->setProperty("id",jsonArray[i].toObject()["id"].toString());

        button->setMinimumHeight(100);
        button->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Maximum);
        button->setFocusPolicy(Qt::NoFocus);
        ui->scrollAreaWidgetContents->layout()->addWidget(button);
    }
}

void StudentsWidget::setGradeName(QString gradeName){
    ui->gradeLabel->setText(gradeName);
}

void StudentsWidget::studentClicked(){
    QString id = QObject::sender()->property("id").toString();
    QString name = QObject::sender()->property("text").toString();
    emit sendStudent(id,name);
}


void StudentsWidget::on_back_clicked()
{
    emit sendClose();
}

void StudentsWidget::updateSize(QSize desktopSize){

    resize(desktopSize);
    ui->frame->resize(desktopSize);
}

void StudentsWidget::on_emergency_clicked()
{
    emit sendDrop();
}
