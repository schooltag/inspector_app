#ifndef DROPWIDGET_H
#define DROPWIDGET_H

#include <QWidget>

namespace Ui {
class DropWidget;
}

class DropWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DropWidget(QWidget *parent = 0);
    ~DropWidget();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

signals:
    sendClose();
    sendDrop();

private:
    Ui::DropWidget *ui;
};

#endif // DROPWIDGET_H
