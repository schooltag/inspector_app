#ifndef STUDENTSWIDGET_H
#define STUDENTSWIDGET_H

#include <QWidget>
#include <QList>
#include <QPushButton>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDesktopWidget>

namespace Ui {
class StudentsWidget;
}

class StudentsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit StudentsWidget(QWidget *parent = 0);
    ~StudentsWidget();
    void populate(QJsonObject jsonObjdata);
    void updateSize(QSize desktopSize);
    void setGradeName(QString gradeName);

private slots:
    void studentClicked();
    void on_back_clicked();

    void on_emergency_clicked();

signals:
    void sendStudent(QString studentId,QString studentName);
    void sendClose();
    void sendDrop();

private:
    Ui::StudentsWidget *ui;
};

#endif // STUDENTSWIDGET_H
