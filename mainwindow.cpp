#include "mainwindow.h"
#include "ui_mainwindow.h"

int min(int a,int b){
    if(a < b) return a;
    else return b;
}

int max(int a,int b){
    if(a > b) return a;
    else return b;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    isOverlayShown = isInfoShown = isConfirmationShown = false;
    deviceId = -1;
    myip = "0.0.0.0";

    ui->setupUi(this);
    ui->label->setText(QString::number(width()) + "x" + QString::number(height()));



    usersWidget = new UsersWidget(ui->centralWidgetMain);
    gradesWigdet = new GradesWidget(ui->centralWidgetMain);
    studentsWidget = new StudentsWidget(ui->centralWidgetMain);
    accessWidget = new AccessWidget(ui->centralWidgetMain);
    confirmationWidget = new ConfirmationWidget(this);
    dropWidget = new DropWidget(this);
    infoWidget = new InfoWidget(this);
    reasonWidget = new ReasonsWidget(this);

    hideOverlay();
    hideInfo();
    hideDrop();
    hideConfirmation();
    hideReason();

    usersWidget->hide();
    gradesWigdet->hide();
    studentsWidget->hide();
    accessWidget->hide();


    connect(usersWidget,SIGNAL(sendLogin(QString,QString,QString)),this,SLOT(login(QString,QString,QString)));
    connect(gradesWigdet,SIGNAL(sendGrade(QString,QString,QString)),this,SLOT(grade(QString,QString,QString)));
    connect(studentsWidget,SIGNAL(sendStudent(QString,QString)),this,SLOT(student(QString,QString)));
    connect(accessWidget,SIGNAL(sendAccess(QString)),this,SLOT(selectAccess(QString)));
    connect(accessWidget,SIGNAL(sendAccess(QString)),this,SLOT(showOverlay()));
    connect(accessWidget,SIGNAL(sendAccess(QString)),this,SLOT(showReason()));


    connect(gradesWigdet,SIGNAL(sendDrop()),this,SLOT(showOverlay()));
    connect(gradesWigdet,SIGNAL(sendDrop()),this,SLOT(showDrop()));
    connect(studentsWidget,SIGNAL(sendDrop()),this,SLOT(showOverlay()));
    connect(studentsWidget,SIGNAL(sendDrop()),this,SLOT(showDrop()));
    connect(accessWidget,SIGNAL(sendDrop()),this,SLOT(showOverlay()));
    connect(accessWidget,SIGNAL(sendDrop()),this,SLOT(showDrop()));

    connect(dropWidget,SIGNAL(sendClose()),this,SLOT(hideOverlay()));
    connect(dropWidget,SIGNAL(sendClose()),this,SLOT(hideDrop()));
    connect(dropWidget,SIGNAL(sendDrop()),this,SLOT(hideOverlay()));
    connect(dropWidget,SIGNAL(sendDrop()),this,SLOT(hideDrop()));
    connect(dropWidget,SIGNAL(sendDrop()),this,SLOT(sendDrop()));


    connect(infoWidget,SIGNAL(sendClose()),this,SLOT(confirmInfo()));
    connect(infoWidget,SIGNAL(sendClose()),this,SLOT(hideInfo()));
    connect(infoWidget,SIGNAL(sendClose()),this,SLOT(hideOverlay()));


    connect(confirmationWidget,SIGNAL(sendClose()),this,SLOT(hideConfirmation()));
    connect(confirmationWidget,SIGNAL(sendClose()),this,SLOT(hideOverlay()));
    connect(confirmationWidget,SIGNAL(sendClose()),this,SLOT(back2Grade()));



    connect(reasonWidget,SIGNAL(sendClose()),this,SLOT(hideReason()));
    connect(reasonWidget,SIGNAL(sendClose()),this,SLOT(hideOverlay()));
    connect(reasonWidget,SIGNAL(sendReason(QString,QString)),this,SLOT(sendReason(QString,QString)));
    connect(reasonWidget,SIGNAL(sendReason(QString,QString)),this,SLOT(hideReason()));
    connect(reasonWidget,SIGNAL(sendReason(QString,QString)),this,SLOT(hideOverlay()));


    connect(gradesWigdet,SIGNAL(sendClose()),this,SLOT(closeGrades()));
    connect(studentsWidget,SIGNAL(sendClose()),this,SLOT(closeStudents()));
    connect(accessWidget,SIGNAL(sendClose()),this,SLOT(closeAccesses()));

    socket = new QTcpSocket();
    timerInit = new QTimer();
    connect(timerInit,SIGNAL(timeout()),this,SLOT(timerInitTimeout()));
    connect(socket,SIGNAL(readyRead()),this,SLOT(readyRead()));
    connect(socket,SIGNAL(connected()),this,SLOT(connected()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(disconnected()));

    loadingGif = new QMovie();
    loadingGif->setFileName(":/res/loading.gif");
    ui->loadingGifLabel->setMovie(loadingGif);
    ui->loadingGifLabel->setGeometry(QDesktopWidget().width()/2 - 100,QDesktopWidget().height()/2 - 100,200,200);
    ui->refreshButton->resize(QDesktopWidget().size());

    receptionTimer = new QTimer();
    connect(receptionTimer,SIGNAL(timeout()),this,SLOT(receptionTimerTimeout()));

    tryToConnect();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::connected(){


    QJsonObject jsonObjMain,jsonObjData;
    QString mac;
    QString ip;

#ifdef Q_OS_WIN
    mac = QNetworkInterface::interfaceFromName("wireless_0").hardwareAddress();
#else //Q_OS_ANDROID
    mac = QNetworkInterface::interfaceFromName("wlan0").hardwareAddress();
#endif

    jsonObjData["deviceMac"] = mac;
    jsonObjData["deviceIp"] = myip;

    jsonObjMain["type"] = "insp_init";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson());

}

void MainWindow::tryToConnect(){

    QHostAddress ip;
    QNetworkInterface ni;
    QString ipString,labelText;
    unsigned int puerto;
    loadingGif->start();
    ui->loadingGifLabel->show();
    ui->refreshButton->hide();

#ifdef Q_OS_WIN
     ni = QNetworkInterface::interfaceFromName("wireless_0");
     puerto = 5740;
#else //Q_OS_ANDROID
    ni = QNetworkInterface::interfaceFromName("wlan0");
    puerto = 5740;
#endif

    if(!ni.isValid()){
        ui->label->setText("Problemas identificando la interfaz de conexion (conectado al wifi?)");
        return;
    }

    for(int i = 0; i < ni.addressEntries().size(); i++){
        ip = ni.addressEntries()[i].ip();
        if(ip.protocol() == QAbstractSocket::IPv4Protocol) break;
    }

    ipString =  ip.toString().split('.')[0] + "." +
                ip.toString().split('.')[1] + "." +
                ip.toString().split('.')[2] + "." +
                "50";                                   //   <---- CAMBIAR A DISCRECIÓN
                //ip.toString().split('.')[3];


#ifdef Q_OS_WIN
    socket->connectToHost(QHostAddress(ipString),puerto);
    //socket->connectToHost(QHostAddress::LocalHost,puerto);
#else //Q_OS_ANDROID
    socket->connectToHost(QHostAddress(ipString),puerto);
#endif

    labelText = QString::number(QDesktopWidget().height()) + "x" + QString::number(QDesktopWidget().width()) + ", " +
                ipString + ":"  + QString::number(puerto)+ ", (Local: " + ni.hardwareAddress() + ", " + ip.toString() + ")";

    ui->label->setText(labelText);

    timerInit->start(6000);

}

void MainWindow::timerInitTimeout(){
    socket->blockSignals(true);
    socket->abort();
    socket->blockSignals(false);
    ui->refreshButton->show();
    ui->loadingGifLabel->hide();
    loadingGif->stop();
}

void MainWindow::disconnected(){
    usersWidget->hide();
    gradesWigdet->hide();
    studentsWidget->hide();
    accessWidget->hide();
    tryToConnect();
}

void MainWindow::receptionTimerTimeout(){
    receptionBuffer.clear();
}


void MainWindow::readyRead(){
    receptionTimer->start(500);
    QByteArray qba = socket->readAll();
    ui->label->setText(QString::number(QDesktopWidget().width()) + "x" + QString::number(QDesktopWidget().height()));

    receptionBuffer.append(qba);
    QJsonObject jsonObjData,jsonObjMain = QJsonDocument::fromJson(receptionBuffer).object();

    QString jsonType = jsonObjMain["type"].toString();

    if(jsonType == "insp_appInit"){
        timerInit->stop();

        jsonObjData = jsonObjMain["data"].toObject();
        deviceId = jsonObjData["deviceId"].toString();

        loadingGif->stop();

        usersWidget->populate(jsonObjData);
        gradesWigdet->populate(jsonObjData);
        accessWidget->populate(jsonObjData);
        usersWidget->show();
        gradesWigdet->hide();
        studentsWidget->hide();
        accessWidget->hide();
        reasonWidget->setReasons(jsonObjData["reasonsList"].toArray());
        return;
    }

    if(jsonType == "insp_info"){
        jsonObjMain = jsonObjMain["data"].toObject();
        infoWidget->populate(jsonObjMain);
        messageId = jsonObjMain["messageId"].toString();
        showInfo();
        showOverlay();
        return;
    }

    if(jsonType == "insp_login"){
        jsonObjMain = jsonObjMain["data"].toObject();
        gradesWigdet->setUserName("Ingesado como " + userName);
        usersWidget->hide();
        gradesWigdet->show();
        studentsWidget->hide();
        accessWidget->hide();
        return;
    }

    if(jsonType == "insp_studentsList"){
        jsonObjMain = jsonObjMain["data"].toObject();
        studentsWidget->populate(jsonObjMain);
        studentsWidget->setGradeName("Curso " + gradeName);
        usersWidget->hide();
        gradesWigdet->hide();
        studentsWidget->show();
        accessWidget->hide();
        return;
    }

    if(jsonType == "insp_accessesList"){
        jsonObjMain = jsonObjMain["data"].toObject();
        accessWidget->populate(jsonObjMain);
        usersWidget->hide();
        gradesWigdet->hide();
        studentsWidget->hide();
        accessWidget->show();
        return;
    }

    if(jsonType == "insp_confirmation"){
        jsonObjMain = jsonObjMain["data"].toObject();
        confirmationWidget->populate(studentName,gradeName,reasonText,jsonObjMain);
        showConfirmation();
        showOverlay();
        back2Grade();
        return;
    }
}


void MainWindow::login(QString userId,QString roleId,QString userName){
    QJsonObject jsonObjMain,jsonObjData;
    this->userId = userId;
    this->roleId = roleId;
    this->userName = userName;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["roleId"] = roleId;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "insp_userLogin";

    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson());
}

void MainWindow::grade(QString gradeId,QString gradeName,QString isEntry){
    QJsonObject jsonObjMain,jsonObjData;
    this->gradeId = gradeId;
    this->isEntry = isEntry;
    this->gradeName = gradeName;
    accessWidget->setEntry(isEntry);

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["roleId"] = roleId;
    jsonObjData["gradeId"] = gradeId;
    jsonObjData["isEntry"] = isEntry;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "insp_gradeSelect";


    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson());
}

void MainWindow::student(QString studentId, QString name){
    this->studentId = studentId;
    this->studentName = name;

    usersWidget->hide();
    gradesWigdet->hide();
    studentsWidget->hide();
    accessWidget->show();

}


void MainWindow::access(QString accessId, QString text, QString id){
    QJsonObject jsonObjMain,jsonObjData;
    this->accessId = accessId;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["gradeId"] = gradeId;
    jsonObjData["studentId"] = studentId;
    jsonObjData["accessId"] = accessId;
    jsonObjData["roleId"] = roleId;
    jsonObjData["reasonId"] = id;
    jsonObjData["isEntry"] = isEntry;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "insp_studentAccess";

    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson());
}

void MainWindow::closeConfirmation(){
    gradesWigdet->unselect();
    usersWidget->hide();
    gradesWigdet->show();
    studentsWidget->hide();
    accessWidget->hide();
}

void MainWindow::closeGrades(){
    usersWidget->show();
    gradesWigdet->hide();
    studentsWidget->hide();
    accessWidget->hide();
}

void MainWindow::closeStudents(){
    usersWidget->hide();
    gradesWigdet->show();
    studentsWidget->hide();
    accessWidget->hide();
}

void MainWindow::closeAccesses(){
    usersWidget->hide();
    gradesWigdet->hide();
    studentsWidget->show();
    accessWidget->hide();
}


void MainWindow::on_refreshButton_clicked()
{
    tryToConnect();
}

void MainWindow::showOverlay(){
    isOverlayShown = true;
    ui->overlay->raise();
    ui->overlay->move(0,0);
}

void MainWindow::hideOverlay(){
    isOverlayShown = false;
    ui->overlay->lower();
    ui->overlay->move(width(),0);
}


void MainWindow::showDrop(){
    isDropShown = true;
    int x = (width() - dropWidget->width())*0.5;
    int y = (height() - dropWidget->height())*0.5;
    dropWidget->move(x,y);

}

void MainWindow::confirmInfo(){
    QJsonObject jsonObjMain,jsonObjData;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["messageId"] = messageId;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "info_info_check";

    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson(QJsonDocument::Compact));
}

void MainWindow::hideInfo(){
    isInfoShown = false;
    infoWidget->move(width(),height());
}

void MainWindow::hideDrop(){
    isDropShown = false;
    dropWidget->move(width(),height());
}

void MainWindow::showInfo(){
    isInfoShown = true;
    int x = (width() - infoWidget->width())*0.5;
    int y = (height() - infoWidget->height())*0.5;
    infoWidget->move(x,y);

}


void MainWindow::showConfirmation(){
    isConfirmationShown = true;
    int x = (width() - confirmationWidget->width())*0.5;
    int y = (height() - confirmationWidget->height())*0.5;
    confirmationWidget->move(x,y);

}

void MainWindow::hideConfirmation(){
    isConfirmationShown = false;
    confirmationWidget->move(width(),height());
}


void MainWindow::back2Grade(){
    gradesWigdet->unselect();
    usersWidget->hide();
    gradesWigdet->show();
    studentsWidget->hide();
    accessWidget->hide();
}

void MainWindow::sendDrop(){
    QJsonObject jsonObjMain,jsonObjData;
    this->userId = userId;
    this->roleId = roleId;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["roleId"] = roleId;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "insp_drop";

    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson());
}


void MainWindow::showReason(){
    isReasonShown = true;
    int x = (width() - reasonWidget->width())*0.5;
    int y = (height() - reasonWidget->height())*0.5;
    reasonWidget->move(x,y);
}

void MainWindow::hideReason(){
    isReasonShown = false;
    reasonWidget->move(width(),height());

}

void MainWindow::sendReason(QString reasonId, QString reasonText){
    QJsonObject jsonObjMain,jsonObjData;
    this->accessId = accessId;
    this->reasonText = reasonText;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["gradeId"] = gradeId;
    jsonObjData["studentId"] = studentId;
    jsonObjData["accessId"] = accessId;
    jsonObjData["roleId"] = roleId;
    jsonObjData["reasonId"] = reasonId;
    jsonObjData["isEntry"] = isEntry;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "insp_studentAccess";

    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson());
}


void MainWindow::selectAccess(QString accessId){
   this->accessId = accessId;
   reasonWidget->setEntry(isEntry);
}

void MainWindow::resizeEvent(QResizeEvent*){
    //QSize desktopSize = QDesktopWidget().size();
    QSize desktopSize = QSize(1000,1000);
    resize(desktopSize);


    ui->centralWidgetMain->resize(desktopSize);
    ui->refreshButton->resize(desktopSize);
    ui->overlay->resize(desktopSize);
    ui->loadingGifLabel->move(width()/2 - 100, height()/2 - 100);

    if(isOverlayShown) showOverlay();
    else hideOverlay();
    if(isDropShown) showDrop();
    else hideDrop();
    if(isInfoShown) showInfo();
    else hideInfo();
    if(isConfirmationShown) showConfirmation();
    else hideConfirmation();
    if(isReasonShown) showReason();
    else hideReason();

    usersWidget->updateSize(desktopSize);
    studentsWidget->updateSize(desktopSize);
    gradesWigdet->updateSize(desktopSize);
    accessWidget->updateSize(desktopSize);
}

