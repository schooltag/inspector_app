#ifndef ACCESSWIDGET_H
#define ACCESSWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QList>
#include <QPushButton>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDesktopWidget>

namespace Ui {
class AccessWidget;
}

class AccessWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AccessWidget(QWidget *parent = 0);
    ~AccessWidget();
    void populate(QJsonObject jsonObj);
    void updateSize(QSize desktopSize);


public slots:
    void accessClicked();
    void clearButtons();
    void addButton(QString labelText,QString id);
    void setEntry(QString isEntry);

signals:
    void sendAccess(QString accessId);
    void sendClose();
    void sendDrop();

private slots:
    void on_back_clicked();

    void on_emergency_clicked();

private:
    Ui::AccessWidget *ui;
    QJsonArray dataArray;
    QString selectedAccess;
    QString isEntry;

};

#endif // ACCESSWIDGET_H
