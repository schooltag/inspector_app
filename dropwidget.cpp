#include "dropwidget.h"
#include "ui_dropwidget.h"

DropWidget::DropWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DropWidget)
{
    ui->setupUi(this);
}

DropWidget::~DropWidget()
{
    delete ui;
}

void DropWidget::on_pushButton_2_clicked()
{
    emit sendClose();
}

void DropWidget::on_pushButton_clicked()
{
    emit sendDrop();
}
