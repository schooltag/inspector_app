#ifndef REASONSWIDGET_H
#define REASONSWIDGET_H

#include <QWidget>
#include <QJsonArray>
#include <QJsonObject>

namespace Ui {
class ReasonsWidget;
}

class ReasonsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ReasonsWidget(QWidget *parent = 0);
    ~ReasonsWidget();
    void addButton(QString text,QString id);
    void clearButtons();
    void setEntry(QString isEntry);
    void setReasons(QJsonArray jsonArray);

private slots:
    void reasonsButtonClicked();
    void on_pushButton_clicked();

private:
    Ui::ReasonsWidget *ui;
    QJsonArray jsonReasonsArray;

signals:
    void sendClose();
    void sendReason(QString,QString);

};

#endif // REASONSWIDGET_H
